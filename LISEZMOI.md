![modderbrothers](./assets/product.png)

# Multi-console Composite Mod

## Description
Offrez à votre Atari 2600, votre Intellivision ou votre console de 1ère génération, une superbe image sur une sortie composite, bien plus simple à connecter que le câble antenne !

Caractéristiques:
* Sortie composite (câbles jaune et blanc)


## Manual
Manuel de montage disponible ici :  [./manual/](./manual/)

## Technical Documents
Les documents techniques sont ici : [./technical-docs/](./technical-docs/)

## Production files
Fichiers de production ici : [./production-files/](./production-files/)

## License & Clause de non-responsabilité

Ce mod original ModderBrothers est distribué sous license **[CERN Open Hardware 2.0 - Faiblement réciproque](./LICENSE.md)** (CERN-OHL-W 2.0) et est susceptible d'évoluer vers des versions supérieures de cette même license.

Concernant les garanties et les responsabilités, voici l'extrait de licence qui s'applique :

##### 6.1 Exclusion de garantie
La Source Couverte et tous les Produits sont fournis "tels quels" et toute garantie expresse ou implicite, y compris, mais non limité aux, garanties implicites de qualité marchande, de qualité satisfaisante, d'absence de droits de tiers, et d'adéquation à un usage ou à un but particulier sont rejetées en ce qui concerne toute Source ou tout produit dans la mesure maximale autorisée par la loi. Le concédant ne fait aucune déclaration selon laquelle une Source ou un Produit ne viole pas ou ne violera pas un brevet, un droit d'auteur, un secret commercial ou un autre droit de propriété. L'intégralité du risque lié à l'utilisation, à la qualité et à la performance de toute Source ou de tout Produit sont à Votre charge et non à celle du Concédant. Cette exclusion de garantie est une partie essentielle de cette Licence et constitue une condition à l'octroi de tous les droits accordés dans le cadre de cette Licence.

##### 6.2 Exclusion et limitation de responsabilité
Le Concédant ne doit, dans la mesure où la loi le permet, avoir aucune responsabilité en matière de dommages directs, indirects, spéciaux, accessoires, consécutifs, exemplaires ou punitifs ou tout autre dommage de quelque nature que ce soit, y compris, sans s’y limiter, l'acquisition de biens ou de services de substitution, la perte d'utilisation, de données ou de profits, ou d'interruption d'activité, quelle que soit la cause et sur toute théorie de contrat, de garantie, de délit civil (y compris la négligence), de responsabilité du produit ou autre, survenant de quelque manière que ce soit en relation avec la Source Couverte, la Source Couverte modifiée et/ou la Fabrication ou la transmission d'un Produit, même si vous avez été informé de l'éventualité de tels dommages, et Vous devrez laisser le(s) Concédant(s) libre(s) et hors de toute responsabilité, coûts, dommages, frais et dépenses, y compris les réclamations de tiers, en relation avec une telle utilisation.

