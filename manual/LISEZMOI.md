![modderbrothers](../assets/product.png)

# Manuel - Mod Composite multi-console

[English Version Here](./README.md)

## Outillage requit

Obligatoire pour l'installation du mod:
- Tournevis cruciforme
- Fer à souder + étain
- Perceuse + foret de 15 ou scie cloche de 15 - scie cloche recommandée

## Installation du Mod

#### Précautions générales

- Toutes les manipulations doivent être effectuées hors tension.
- Vous êtes responsables de votre outillage et de ses manipulations. Si vous n'avez pas les aptitudes et/ou l'expérience requise, nous vous conseillons de faire réaliser ces manipulations par une personne qualifiée.

#### Précautions à prendre avant de toucher les cartes et les composants électroniques

Certains composants sont très sensibles à l'électricité statique.
Veillez à vous décharger avant l'ouverture de votre :
- Placez-vous dans une piece en carrelage, et évitez la moquette!
- Évitez les pulls en laine ou autres vêtements qui se chargent facilement!
- Touchez la carcasse métallique d'un appareil électrique pour vous décharger à la terre.

#### Ouverture de la console

Pour l'atari 2600, retournez là, et retirez les 4 vis. Puis retirez le capot bas, la carte mère est maintenue par le haut du capot.

Pour l'intellivision, retirez les vis sous la console, puis remettez là droite avant de retirer le capot supperieur. Ensuite retirez les 6 vis qui maintiennent la barre plastique au milieu, pour dégager complètement la carte mère.
Vous pouvez sortir la carte mère de la console. Faite attention à marquer les connecteur des pads et à marquer les sens de connexion (une trace au marquer noir sur le connecteur male et le connecteur femelle permetront de ne pas se tromper au remontage)

#### Installation du mod sur Atari 2600.

Vous avez à votre disposition 4 fils dupont. Il vous faudra couper les connecteurs dupont d'un coté, puis dénuder les fils. L'ordre des fils importe peu, vous pouvez souder les 4 points selon vos convenances et les codes couleurs qui vous interessent.
Il sera très simple après de brancher les fils dans n'importe quel ordre sur le mod.

Dans cette console, le Mod composite va se fixer au fond de la console (capot bas) à l'aide d'un scotch double face ou d'un pistolet à colle selon ce que vous avez sous la main.

#### Installation du mod sur Intellivision

Le mod pourra prendre la place du Modulateur RF, qu'il faudra dessouder au préalable, ou débrancher selon les modèles.

# W.I.P

Rejoignez nous sur Discord pour une aide au montage: https://discord.gg/TB6KQ4g (chanel #modder-brothers)